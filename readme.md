

levanta jade con un agente

```bash
./gradlew runJadeWithAgent -Dagent=agentName:multiAgents.agents.gui.MyGuiAgent
```

levata jade remoto
```bash
./gradlew runRemoteJade -Dhost=HOST port=PORT
```


levata jade con Launcher
```bash
./gradlew runJadeWithLauncher
```


levata jade remoto
```bash
./gradlew runRemoteJade -Dhost=HOST port=PORT
```


