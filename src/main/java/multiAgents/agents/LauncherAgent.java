package multiAgents.agents;

import jade.core.Agent;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

/**
 * Created by frank on 10/17/15.
 */
public class LauncherAgent extends Agent {


    @Override
    protected void setup(){

        AgentContainer agentContainer = getContainerController();
        try {

            AgentController supermarket = agentContainer.createNewAgent(
                    "Supermarket",
                    "multiAgents.agents.MarketAgent",
                    null
            );

            supermarket.start();
            AgentController fridge1 = agentContainer.createNewAgent(
                    "fridgeA",
                    "multiAgents.agents.RefrigeratorAgent",
                    new Object[]{"1"}
            );

            fridge1.start();

            AgentController fridge2 = agentContainer.createNewAgent(
                    "fridgeB",
                    "multiAgents.agents.RefrigeratorAgent",
                    new Object[]{"3"}
            );

            fridge2.start();
        }
        catch (StaleProxyException spe){
            System.out.println("algo rompio");
            spe.printStackTrace();
        }
    }
}
