package multiAgents.agents;

import jade.core.Agent;
import multiAgents.behaviours.refrigerator.BuyProductBehaviour;

/**
 * Created by frank on 9/20/15.
 */
public class RefrigeratorAgent extends Agent {

    @Override
    protected void setup() {
        System.out.print("Refrigerator Agent: " + this.getName() + " Started");

        String productId = (String) getArguments()[0];
        addBehaviour(new BuyProductBehaviour(productId));
    }

    @Override
    protected void takeDown() {
        System.out.print("Refrigerator Agent: " + this.getName() + " Stopped");
    }
}
