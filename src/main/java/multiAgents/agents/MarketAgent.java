package multiAgents.agents;

import jade.core.Agent;
import multiAgents.behaviours.market.SellBehaviour;
import multiAgents.behaviours.market.WaitForProductQueryBehaviour;
import multiAgents.utils.Product;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by frank on 10/4/15.
 */
public class MarketAgent extends Agent {

    private Map<String, Product> products;

    public MarketAgent() {
        setup();
        products = new HashMap<>();
        populateProducts();
    }

    private void populateProducts() {
        products.put("1", new Product(1L, 5.8D, 0));
        products.put("2", new Product(2L, 6.8D, 60));
        products.put("3", new Product(3L, 7.8D, 70));
        products.put("4", new Product(4L, 8.8D, 90));
    }

    @Override
    protected void setup() {
        System.out.print("Market Agent: " + this.getName() + " Started");

        addBehaviour(new WaitForProductQueryBehaviour());
        addBehaviour(new SellBehaviour());
    }

    @Override
    protected void takeDown() {
        System.out.println("Market Agent: " + this.getName() + " Stopped");
    }

    public Product getProduct(String productId) {
        return products.get(productId);
    }

    public boolean productAvailable(String productId, Integer quantity) {
        Product product = products.get(productId);
        return product != null && product.getQuantity() >= quantity;
    }

    public void reduceProductQuantityBy(String productId, Integer quantity) {
        try {
            getProduct(productId).reduceQuantityBy(quantity);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
