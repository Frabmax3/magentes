package multiAgents.behaviours.refrigerator;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.FSMBehaviour;
import multiAgents.behaviours.EndStateBahaviour;

/**
 * Created by frank on 10/4/15.
 */
public class BuyProductBehaviour extends FSMBehaviour{

    private static final long serialVersionUID = -1544928812157021187L;

    private static final String QUERY_FOR_PRODUCT_INFO = "queryProductInfo";
    private static final String WAIT_FOR_PRODUCT_INFO = "waitForProductInfo";
    private static final String ATTEMPT_TO_BUY_PRODUCT = "buyProduct";
    private static final String WAIT_FOR_CONFIRM = "waitForConfirm";
    private static final String END_STATE = "endState";

    protected static final int SUCCESSFUL_PURCHASE = 1;
    protected static final int UNSUCCESSFUL_PURCHASE = 0;
    protected static final String PRODUCTID_KEY = "productId_key";

    protected static final String LAST_MESSAGE =  "lastMessage";
    protected static final String MARKETID = "marketId";


    private final String productId;


    public BuyProductBehaviour(String productId){
        super();

        this.productId = productId;

        setupFSM();
    }

    private void setupFSM() {
        DataStore dataStore = new DataStore();
        
        dataStore.put(PRODUCTID_KEY, productId);

        Behaviour queryProductInfoState = new QueryProductInfoBehaviour();
        queryProductInfoState.setDataStore(dataStore);
        registerFirstState(queryProductInfoState, QUERY_FOR_PRODUCT_INFO);

        Behaviour waitForProductInfoState = new WaitForProductInfoBehviour();
        waitForProductInfoState.setDataStore(dataStore);
        registerState(waitForProductInfoState, WAIT_FOR_PRODUCT_INFO);

        Behaviour buyProductState = new AttemptToBuyProductBehaviour();
        buyProductState.setDataStore(dataStore);
        registerState(buyProductState, ATTEMPT_TO_BUY_PRODUCT);

        Behaviour waitForConfirmationState = new WaitForConfirmationState();
        waitForConfirmationState.setDataStore(dataStore);
        registerState(waitForConfirmationState, WAIT_FOR_CONFIRM);

        Behaviour endState = new EndStateBahaviour();
        registerLastState(endState, END_STATE);

        registerDefaultTransition(QUERY_FOR_PRODUCT_INFO, WAIT_FOR_PRODUCT_INFO);
        registerDefaultTransition(WAIT_FOR_PRODUCT_INFO, ATTEMPT_TO_BUY_PRODUCT);
        registerDefaultTransition(ATTEMPT_TO_BUY_PRODUCT, WAIT_FOR_CONFIRM);

        registerTransition(WAIT_FOR_CONFIRM, END_STATE, SUCCESSFUL_PURCHASE);
        registerTransition(WAIT_FOR_CONFIRM, ATTEMPT_TO_BUY_PRODUCT, UNSUCCESSFUL_PURCHASE);

    }

}
