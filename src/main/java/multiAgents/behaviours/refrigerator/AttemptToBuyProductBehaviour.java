package multiAgents.behaviours.refrigerator;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

/**
 * Created by frank on 10/4/15.
 */
public class AttemptToBuyProductBehaviour extends Behaviour {

    @Override
    public void action() {

        ACLMessage lastMessage = (ACLMessage) getDataStore().get(BuyProductBehaviour.LAST_MESSAGE);
        String conversationId = lastMessage.getConversationId();
        AID marketId = (AID) getDataStore().get(BuyProductBehaviour.MARKETID);

        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.setConversationId(conversationId);
        request.addReceiver(marketId);
        request.setContent(buildMessageContent());

        myAgent.send(request);
    }

    @Override
    public boolean done() {
        return true;
    }

    private String buildMessageContent() {
        return ""+
                getDataStore().get(BuyProductBehaviour.PRODUCTID_KEY)+
                ","
                +1;
    }

}
