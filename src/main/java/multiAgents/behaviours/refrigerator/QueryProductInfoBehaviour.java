package multiAgents.behaviours.refrigerator;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

import static multiAgents.utils.Constants.*;

/**
 * Created by frank on 10/4/15.
 */
public class QueryProductInfoBehaviour extends Behaviour {


    private static int conversationCount = 0;

    public QueryProductInfoBehaviour() {
        super();
    }

    @Override
    public void action() {
        getDataStore().put(BuyProductBehaviour.MARKETID, findMarketId());
        ACLMessage message = createMessage();

        myAgent.send(message);

        getDataStore().put(BuyProductBehaviour.LAST_MESSAGE, message);
    }

    @Override
    public boolean done() {
        return true;
    }

    private ACLMessage createMessage() {
        String productId = (String) getDataStore().get(BuyProductBehaviour.PRODUCTID_KEY);
        AID marketId = (AID) getDataStore().get(BuyProductBehaviour.MARKETID);

        ACLMessage message = new ACLMessage(ACLMessage.QUERY_IF);
        message.addReceiver(marketId);
        message.setLanguage(LANGUAGE);
        message.setContent(productId);
        message.setConversationId(getNextConversationId());

        return message;
    }

    private AID findMarketId() {
        return new AID(SUPERMARKET, AID.ISLOCALNAME);
    }

    private String getNextConversationId() {
        return String.valueOf(conversationCount++);
    }
}
