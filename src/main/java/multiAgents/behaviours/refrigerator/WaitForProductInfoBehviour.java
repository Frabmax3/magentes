package multiAgents.behaviours.refrigerator;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Created by frank on 10/4/15.
 */
public class WaitForProductInfoBehviour extends Behaviour {

    private boolean done = false;

    @Override
    public void action() {

        ACLMessage lastMessage = (ACLMessage) getDataStore().get(BuyProductBehaviour.LAST_MESSAGE);
        String conversationId = lastMessage.getConversationId();

        MessageTemplate messageTemplate = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                MessageTemplate.MatchConversationId(conversationId)
        );

        ACLMessage productInfo = myAgent.receive(messageTemplate);

        if (productInfo != null) {
            done = true;
            System.out.println(productInfo);
        } else {
            block();
        }
    }

    @Override
    public boolean done() {
        return done;
    }
}
