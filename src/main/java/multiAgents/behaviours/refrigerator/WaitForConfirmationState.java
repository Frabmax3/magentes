package multiAgents.behaviours.refrigerator;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Created by frank on 10/4/15.
 */
public class WaitForConfirmationState extends Behaviour {

    private boolean done = false;
    private int transactionToGo = 0;

    @Override
    public void action() {

        ACLMessage lastMessage = (ACLMessage) getDataStore().get(BuyProductBehaviour.LAST_MESSAGE);
        String conversationId = lastMessage.getConversationId();

        MessageTemplate replyTemplate = MessageTemplate.and(
                MessageTemplate.MatchConversationId(conversationId),
                MessageTemplate.or(
                        MessageTemplate.MatchPerformative(ACLMessage.AGREE),
                        MessageTemplate.MatchPerformative(ACLMessage.REFUSE)
                )
        );

        ACLMessage reply = myAgent.receive(replyTemplate);
        if(reply != null){
            done = true;
            if(reply.getPerformative() == ACLMessage.AGREE){
                transactionToGo = BuyProductBehaviour.SUCCESSFUL_PURCHASE;
            }else {
                transactionToGo = BuyProductBehaviour.UNSUCCESSFUL_PURCHASE;
            }
        }else {
            block();
        }

    }

    @Override
    public boolean done() {
        return done;
    }

    @Override
    public int onEnd(){
        return transactionToGo;
    }
}
