package multiAgents.behaviours;

import jade.core.behaviours.OneShotBehaviour;

/**
 * Created by frank on 10/4/15.
 */
public class EndStateBahaviour extends OneShotBehaviour {
    @Override
    public void action() {

        System.out.println("Transaction finished");
    }

}
