package multiAgents.behaviours.market;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import multiAgents.agents.MarketAgent;
import multiAgents.utils.Product;

/**
 * Created by frank on 10/17/15.
 */
public class WaitForProductQueryBehaviour extends Behaviour {
    private static final String INVALID_PRODUCT = "Invalid Product";

    @Override
    public void action() {

        MessageTemplate messageTemplate = MessageTemplate.MatchPerformative(ACLMessage.QUERY_IF);

        ACLMessage queryProductMsg = myAgent.receive(messageTemplate);
        if(queryProductMsg != null){
            ACLMessage reply = queryProductMsg.createReply();

            Product product = findProductInfo(queryProductMsg.getContent());

            if(product != null){
                reply.setPerformative(ACLMessage.INFORM);
                reply.setContent(product.getInformation());
            }else {
                reply.setPerformative(ACLMessage.FAILURE);
                reply.setContent(INVALID_PRODUCT);
            }

            myAgent.send(reply);
        }else {
            block();
        }

    }


    private Product findProductInfo(String content) {
        return ((MarketAgent)myAgent).getProduct(content);
    }

    @Override
    public boolean done() {
        return false;
    }
}
