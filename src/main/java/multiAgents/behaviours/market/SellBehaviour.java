package multiAgents.behaviours.market;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import multiAgents.agents.MarketAgent;

/**
 * Created by frank on 10/4/15.
 */
public class SellBehaviour extends Behaviour {

    protected static final String CONVERSATION_ID_KEY = "conversation_id_key";
    private static final String WAIT_FOR_QUERY = "waitForQuery";

    public SellBehaviour() {
        super();
    }

    @Override
    public void action() {
        MessageTemplate requestTemplate = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);

        ACLMessage requestMessage = myAgent.receive(requestTemplate);
        if (requestMessage != null) {
            processRequest(requestMessage);
        } else {
            block();
        }
    }

    private void processRequest(ACLMessage requestMessage) {
        try {
            String content = requestMessage.getContent();
            String[] values = content.split(",");
            String productId = values[0];
            Integer quantity = Integer.valueOf(values[1]);

            ACLMessage reply = requestMessage.createReply();

            if (((MarketAgent) myAgent).productAvailable(productId, quantity)) {
                ((MarketAgent) myAgent).reduceProductQuantityBy(productId, quantity);

                reply.setPerformative(ACLMessage.AGREE);
            } else {
                reply.setPerformative(ACLMessage.REFUSE);
            }

            myAgent.send(reply);

        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Invalid Content message");
        }

    }

    @Override
    public boolean done() {
        return false;
    }
}
