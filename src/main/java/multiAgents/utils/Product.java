package multiAgents.utils;


import jade.util.leap.Serializable;

/**
 * Created by frank on 10/4/15.
 */
public class Product implements Serializable {

    private static final long serialVersionUID = 6388914630768311183L;

    private Long productId;
    private Double price;
    private int quantity;

    public Product(Long productId, Double price, int quantity){
        this.productId = productId;
        this.price = price;
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public Double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getInformation(){
        return ""+price+","+quantity;
    }

    public void reduceQuantityBy(Integer quantity) throws Exception {
        if(quantity> this.quantity){
            throw  new Exception("Invalid quantity");
        }else{
            this.quantity -= quantity;
        }

    }
}
